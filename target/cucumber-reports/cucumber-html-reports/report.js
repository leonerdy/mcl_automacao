$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/1_COMPRAS/A_Cadastro.feature");
formatter.feature({
  "name": "Cadastro Requisição Compra",
  "description": "",
  "keyword": "Funcionalidade"
});
formatter.scenarioOutline({
  "name": "",
  "description": "",
  "keyword": "Esquema do Cenário",
  "tags": [
    {
      "name": "@InclusãoCadastro"
    }
  ]
});
formatter.step({
  "name": "que acesso ao menu compras, submenu requisição compra e ao submenu cadastro",
  "keyword": "Dado "
});
formatter.step({
  "name": "clico no botão nova requisição de compra",
  "keyword": "Quando "
});
formatter.step({
  "name": "seleciono o combobox tipo objeto, preencho os comboboxes almoxarifado \"\u003calmoxarifado\u003e\", órgão \"\u003corgao\u003e\", destino \"\u003cdestino\u003e\" e requisitante \"\u003crequisitante\u003e\"",
  "keyword": "Quando "
});
formatter.step({
  "name": "preencho o campo numero requisição origem \"\u003crequisicaoOrigem\u003e\", ano \"\u003cano\u003e\", preencho o combobox registro de preço \"\u003cregistroPreco\u003e\", local de entrega \"\u003clocalEntrega\u003e\" e observações \"\u003cobservacoes\u003e\"",
  "keyword": "E "
});
formatter.step({
  "name": "clico no botão adicionar item, seleciono o combobox tipo do item, preencho o combobox material \"\u003cmaterial\u003e\", preencho o campo quantidade \"\u003cquantidade\u003e\", valor unitário \"\u003cvalorUnitario\u003e\", detalhamento \"\u003cdetalhamento\u003e\" e clico no botão salvar popUp e salvar inclusão",
  "keyword": "E "
});
formatter.step({
  "name": "recebo uma mensagem de inclusão \"\u003cmensagem\u003e\"",
  "keyword": "Então "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Exemplos",
  "rows": [
    {
      "cells": [
        "almoxarifado",
        "orgao",
        "destino",
        "requisitante",
        "requisicaoOrigem",
        "ano",
        "registroPreco",
        "localEntrega",
        "observacoes",
        "material",
        "quantidade",
        "valorUnitario",
        "detalhamento",
        "mensagem"
      ]
    },
    {
      "cells": [
        "Central",
        "SECRETARIA MUNICIPAL DE GOVERNO",
        "Escola Municipal Esperança",
        "Usuário Conam",
        "1",
        "2020",
        "1",
        "1",
        "TESTE",
        "Laranja",
        "1500",
        "1500",
        "TESTE",
        ""
      ]
    }
  ]
});
formatter.scenario({
  "name": "",
  "description": "",
  "keyword": "Esquema do Cenário",
  "tags": [
    {
      "name": "@InclusãoCadastro"
    }
  ]
});
formatter.step({
  "name": "que acesso ao menu compras, submenu requisição compra e ao submenu cadastro",
  "keyword": "Dado "
});
formatter.match({
  "location": "CadastroStep.que_acesso_ao_menu_compras_submenu_requisição_compra_e_ao_submenu_cadastro()"
});
formatter.result({
  "status": "passed"
});
formatter.afterstep({
  "status": "passed"
});
formatter.step({
  "name": "clico no botão nova requisição de compra",
  "keyword": "Quando "
});
formatter.match({
  "location": "CadastroStep.clico_no_botão_nova_requisição_de_compra()"
});
formatter.result({
  "status": "passed"
});
formatter.afterstep({
  "status": "passed"
});
formatter.step({
  "name": "seleciono o combobox tipo objeto, preencho os comboboxes almoxarifado \"Central\", órgão \"SECRETARIA MUNICIPAL DE GOVERNO\", destino \"Escola Municipal Esperança\" e requisitante \"Usuário Conam\"",
  "keyword": "Quando "
});
formatter.match({
  "location": "CadastroStep.seleciono_o_combobox_tipo_objeto_preencho_os_comboboxes_almoxarifado_órgão_destino_e_requisitante(String,String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.afterstep({
  "status": "passed"
});
formatter.step({
  "name": "preencho o campo numero requisição origem \"1\", ano \"2020\", preencho o combobox registro de preço \"1\", local de entrega \"1\" e observações \"TESTE\"",
  "keyword": "E "
});
formatter.match({
  "location": "CadastroStep.preencho_o_campo_numero_requisição_origem_ano_preencho_o_combobox_registro_de_preço_local_de_entrega_e_observações(String,String,String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.afterstep({
  "status": "passed"
});
formatter.step({
  "name": "clico no botão adicionar item, seleciono o combobox tipo do item, preencho o combobox material \"Laranja\", preencho o campo quantidade \"1500\", valor unitário \"1500\", detalhamento \"TESTE\" e clico no botão salvar popUp e salvar inclusão",
  "keyword": "E "
});
formatter.match({
  "location": "CadastroStep.clico_no_botão_adicionar_item_seleciono_o_combobox_tipo_do_item_preencho_o_combobox_material_preencho_o_campo_quantidade_valor_unitário_detalhamento_e_clico_no_botão_salvar_popUp_e_salvar_inclusão(String,String,String,String)"
});
formatter.result({
  "error_message": "java.lang.Exception: \r\n\tat br.conam.core.Base.VerificaMensagemPopUp(Base.java:417)\r\n\tat br.conam.steps.compras.Requisição.Compras.CadastroStep.clico_no_botão_adicionar_item_seleciono_o_combobox_tipo_do_item_preencho_o_combobox_material_preencho_o_campo_quantidade_valor_unitário_detalhamento_e_clico_no_botão_salvar_popUp_e_salvar_inclusão(CadastroStep.java:90)\r\n\tat ✽.clico no botão adicionar item, seleciono o combobox tipo do item, preencho o combobox material \"Laranja\", preencho o campo quantidade \"1500\", valor unitário \"1500\", detalhamento \"TESTE\" e clico no botão salvar popUp e salvar inclusão(src/test/resources/1_COMPRAS/A_Cadastro.feature:9)\r\n",
  "status": "failed"
});
formatter.write("Evidência");
formatter.embedding("image/png", "embedded0.png");
formatter.afterstep({
  "status": "passed"
});
formatter.step({
  "name": "recebo uma mensagem de inclusão \"\"",
  "keyword": "Então "
});
formatter.match({
  "location": "CadastroStep.recebo_uma_mensagem_de_inclusão(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.afterstep({
  "status": "skipped"
});
});