package br.conam.steps.compras.Requisição.Compras;

import org.openqa.selenium.By;

import br.conam.core.Base;
import cucumber.api.java.es.Dado;
import cucumber.api.java.it.Quando;
import cucumber.api.java.pt.Então;

public class CadastroStep extends Base {
	
	
	/**************** LOCATORS INCLUSÃO ****************/
	
	private static By acessoMenu = By.xpath("//li[2]/a/i");
	private static By acessoSubMenu1 = By.xpath("//li[2]/ul/li/a/span");
	private static By acessoSubMenu2 = By.linkText("Cadastro");
	private static By botaoIncluir = By.linkText("Nova Requisição de Compra");
	private static By selecaotipoObjeto = By.xpath("//label[2]/div/div[3]/span");
	private static By itemtipoObjeto = By.id("requisicaoCompraDados:requisicaoCompraForm:tipoObjeto_1");
	private static By campoalmoxarifado = By.id("requisicaoCompraDados:requisicaoCompraForm:almoxarifado:almoxarifado:almoxarifado_input");
	private static By itemalmoxarifado = By.id("requisicaoCompraDados:requisicaoCompraForm:almoxarifado:almoxarifado:j_idt77");
	private static By campoorgao = By.id("requisicaoCompraDados:requisicaoCompraForm:orgao:orgao:orgao_input");
	private static By itemorgao = By.id("requisicaoCompraDados:requisicaoCompraForm:orgao:orgao:j_idt92");
	private static By campodestino = By.id("requisicaoCompraDados:requisicaoCompraForm:destino:destino:destino_input");
	private static By itemdestino = By.id("requisicaoCompraDados:requisicaoCompraForm:destino:destino:j_idt103");
	private static By camporequisitante = By.id("requisicaoCompraDados:requisicaoCompraForm:servidor:servidor:servidor_input");
	private static By itemrequisitante = By.id("requisicaoCompraDados:requisicaoCompraForm:servidor:servidor:j_idt127");
	private static By camponumeroRequisicaoOrigem = By.id("requisicaoCompraDados:requisicaoCompraForm:numeroReqOrigem:numeroReqOrigem_input");
	private static By campoano = By.id("requisicaoCompraDados:requisicaoCompraForm:anoReqOrigem:anoReqOrigem_input");
	private static By camporegistroPreco = By.id("requisicaoCompraDados:requisicaoCompraForm:registroPrecos:registroPrecos:registroPrecos_input");
	private static By itemregistroPreco = By.id("requisicaoCompraDados:requisicaoCompraForm:registroPrecos:registroPrecos:j_idt153");
	private static By campolocalEntrega = By.id("requisicaoCompraDados:requisicaoCompraForm:localEntrega:localEntrega:localEntrega_input");
	private static By itemlocalEntrega = By.id("requisicaoCompraDados:requisicaoCompraForm:localEntrega:localEntrega:j_idt169");
	private static By campoobservacoes = By.id("requisicaoCompraDados:requisicaoCompraForm:observacao");
	private static By botaoadicionarItem = By.id("requisicaoCompraDados:requisicaoCompraForm:j_idt176");
	private static By selecaotipoItem = By.xpath("//div/div/div[3]/span");
	private static By itemtipoItem = By.id("editItemRequisicaoCompra:formPopupEditItemRequisicao:tipoItem_0");
	private static By campomaterial = By.id("editItemRequisicaoCompra:formPopupEditItemRequisicao:material:material:material_input");
	private static By itemmaterial = By.id("editItemRequisicaoCompra:formPopupEditItemRequisicao:material:material:j_idt252");
	private static By campoquantidade = By.id("editItemRequisicaoCompra:formPopupEditItemRequisicao:quantidadeInicial:quantidadeInicial_input");
	private static By campovalorUnitario = By.id("editItemRequisicaoCompra:formPopupEditItemRequisicao:valorUnitarioInicial:valorUnitarioInicial_input");
	private static By campodetalhamento = By.id("editItemRequisicaoCompra:formPopupEditItemRequisicao:detalhamento");
	private static By botaosalvarpopUp = By.id("editItemRequisicaoCompra:formPopupEditItemRequisicao:j_idt322");
	private static By botaosalvar = By.linkText("Salvar");
	private static By mensagemsucesso = By.xpath("");
	
	
	/**************** Metodos Inclusão Cadastro ****************/
	
	@Dado("que acesso ao menu compras, submenu requisição compra e ao submenu cadastro")
	public void que_acesso_ao_menu_compras_submenu_requisição_compra_e_ao_submenu_cadastro() throws Exception {
	    this.chamaCaminho5pDefinindoTipoLocator(acessoMenu, acessoSubMenu1, acessoSubMenu2, null, null);
	}

	@Quando("clico no botão nova requisição de compra")
	public void clico_no_botão_nova_requisição_de_compra() throws Exception {
		this.AcessoInclusão(botaoIncluir);
	}

	@Quando("seleciono o combobox tipo objeto, preencho os comboboxes almoxarifado {string}, órgão {string}, destino {string} e requisitante {string}")
	public void seleciono_o_combobox_tipo_objeto_preencho_os_comboboxes_almoxarifado_órgão_destino_e_requisitante(String almoxarifado, String orgao, String destino, String requisitante) throws Exception {
	    this.selecionaComboBox(selecaotipoObjeto, itemtipoObjeto);
	    this.escreverId(campoalmoxarifado, almoxarifado, itemalmoxarifado);
	    this.escreverId(campoorgao, orgao, itemorgao);
	    this.escreverId(campodestino, destino, itemdestino);
	    this.escreverId(camporequisitante, requisitante, itemrequisitante);
	}

	@Quando("preencho o campo numero requisição origem {string}, ano {string}, preencho o combobox registro de preço {string}, local de entrega {string} e observações {string}")
	public void preencho_o_campo_numero_requisição_origem_ano_preencho_o_combobox_registro_de_preço_local_de_entrega_e_observações(String requisicaoOrigem, String ano, String registroPreco, String localEntrega, String observacoes) throws Exception {
	    this.escrever(camponumeroRequisicaoOrigem, requisicaoOrigem);
	    this.escrever(campoano, ano);
	    this.escreverId(camporegistroPreco, registroPreco, itemregistroPreco);
	    this.escreverId(campolocalEntrega, localEntrega, itemlocalEntrega);
	    this.escrever(campoobservacoes, observacoes);
	}

	@Quando("clico no botão adicionar item, seleciono o combobox tipo do item, preencho o combobox material {string}, preencho o campo quantidade {string}, valor unitário {string}, detalhamento {string} e clico no botão salvar popUp e salvar inclusão")
	public void clico_no_botão_adicionar_item_seleciono_o_combobox_tipo_do_item_preencho_o_combobox_material_preencho_o_campo_quantidade_valor_unitário_detalhamento_e_clico_no_botão_salvar_popUp_e_salvar_inclusão(String material, String quantidade, String valorUnitario, String detalhamento) throws Exception {
	    this.clicar(botaoadicionarItem);
	    waitBase(2);
	    this.selecionaComboBox(selecaotipoItem, itemtipoItem);
	    this.escreverId(campomaterial, material, itemmaterial);
	    this.escrever(campoquantidade, quantidade);
	    this.escrever(campovalorUnitario, valorUnitario);
	    this.escrever(campodetalhamento, detalhamento);
	    waitBase(2);
	    this.clicar(botaosalvarpopUp);
	    this.VerificaMensagemPopUp(botaosalvar);
	}

	@Então("recebo uma mensagem de inclusão {string}")
	public void recebo_uma_mensagem_de_inclusão(String mensagem) throws Exception {
	    this.VerificaMensagemSucesso(mensagemsucesso, mensagem);
	}
}