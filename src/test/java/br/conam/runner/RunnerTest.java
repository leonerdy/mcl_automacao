package br.conam.runner;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import br.conam.core.DriverFactory;
import br.conam.core.Inicializacao;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src/test/resources/",
		glue = "br.conam.steps",
		monochrome = true,
		tags = "@InclusãoCadastro",
	    plugin = {"pretty", "html:target/cucumber-reports/cucumber-html-reports","json:target/reports/CucumberReport.json"}, //"de.monochromata.cucumber.report.PrettyReports:target/pretty-cucumber"},
		snippets = SnippetType.CAMELCASE,
		dryRun = false,
		strict = false
		)
	
	public class RunnerTest{
	@BeforeClass
	public static void inicializa() throws Exception {
		new Inicializacao();
	}
	@AfterClass
	public static void finaliza() {
		DriverFactory.killDriver();
	}
}