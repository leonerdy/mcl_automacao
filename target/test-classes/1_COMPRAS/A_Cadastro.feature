#language: pt
Funcionalidade: Cadastro Requisição Compra
@InclusãoCadastro
Esquema do Cenário: 
  Dado que acesso ao menu compras, submenu requisição compra e ao submenu cadastro 
  Quando clico no botão nova requisição de compra 
  Quando seleciono o combobox tipo objeto, preencho os comboboxes almoxarifado "<almoxarifado>", órgão "<orgao>", destino "<destino>" e requisitante "<requisitante>"
  E preencho o campo numero requisição origem "<requisicaoOrigem>", ano "<ano>", preencho o combobox registro de preço "<registroPreco>", local de entrega "<localEntrega>" e observações "<observacoes>"
  E clico no botão adicionar item, seleciono o combobox tipo do item, preencho o combobox material "<material>", preencho o campo quantidade "<quantidade>", valor unitário "<valorUnitario>", detalhamento "<detalhamento>" e clico no botão salvar popUp e salvar inclusão
  Então recebo uma mensagem de inclusão "<mensagem>"
Exemplos:
  |almoxarifado|orgao                          |destino                   |requisitante |requisicaoOrigem|ano |registroPreco|localEntrega|observacoes|material|quantidade|valorUnitario|detalhamento|mensagem|
  |Central     |SECRETARIA MUNICIPAL DE GOVERNO|Escola Municipal Esperança|Usuário Conam|1               |2020|1            |1           |TESTE      |Laranja |1500      |1500         |TESTE       |        |